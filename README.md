# 2018-odc-final
This is the final submission for the 2018 OpenDataDE Data Jam.

**Team Name: Commute+**

# Background
## The problem

Hey Delawareans, have you ever thought of biking to work?!

Bike-commuting is all the rage these days.  It's eco-friendly. It's healthly. It's Cheap.  I bet everyone is doing it!

Actually...  most people, both nationally and here in DE, are not.  [Data from the from the American Community Survery](https://datausa.io/profile/geo/delaware/) conducted by the US Census Bureau shows that 89% of Delawareans drove to work and another 9% carpooled.  How many People biked to work?  Only 0.3%!


This is a striking revelation for a state that prides itself on being bike-friendly.  Delaware has made great strides in recent years to improve bicycle safety and [build bicycle infrastructure](http://www.bikede.org/2016/09/23/delawares-biggest-bike-project/), including Governor Carney signing into law the [Bicycle Friendly Delaware Act](http://www.bikede.org/2017/10/03/bicycle-friendly-delaware-act/) last fall and the upcoming completion of the bridge that will finish off the [Wilmington-New Castle Greenway](http://www.bikede.org/2016/09/23/delawares-biggest-bike-project/) in New Castle County.  With efforts like these, DE placed 7th in a [nationwide ranking of the most bike-friendly states](http://bikeleague.org/content/ranking) in 2017, peaking as high as 3rd in 2015. Given all the state has done to encourage cycling, why aren't more people biking to work?


There is a lot to consider for someone who wants to make the switch from driving to biking.  The avereage vehicular commute time for Delawareans is [25 minutes](https://datausa.io/profile/geo/delaware/).  Even if we assume a modest 10-mile distance is covered in those 25 minutes, that is still makes for a 20 mile round-trip biek ride on top of your regukar workday.  For someone who isn't a conditioned cyclist, this would be quite the endeavor to take on for a even a single day a week.  How long would it take?  Could you even make it?  What roads would you even take?  You certainly aren't going to be biking on I-95!

## Solution

**Enter commute+**

What if you could replace your vehicluar **commute** with biking **+** taking public transport?  What if 80% of the comute distance is covered by DART bus, and you only have to bike a mile from home to the bus stop, then another mile from the bus stop to work at the other end of the trip?  This is much more reasonable for someone who wants to begin incorporating biking into their daily routine -- getting some fresh air each day, being more activity, and helping out the enviroment.  The route planning solution we have provided will do just that!

# How it works

In the spirit of Open Data Delaware, we are committed to using open source data and software packages in order to build our final product.  There are five key components needed to deliver the final prototype.  On the data side of things are openstreetmap and DelDOT tranist feeds.  One the software side are R5, Modeify, and ModeifyReactNative.

## OpenStreetMap

[OpenStreetMap](https://www.openstreetmap.org/#map=5/38.007/-95.844) provides similar functionality to Google Maps, but is entirely free and is maintained by the community. [Effectively the map analog to wikipedia](https://en.wikipedia.org/wiki/OpenStreetMap), anyone go in and make changes to the base map.  This is particulary useful for tagging points fo interst or utilities that aren't incorporated into other map softwares like google maps.  For example, the community can add tags for bike rack locations that will be available other users to see.  Another useful feature is the ability to tag roads as bike-friendly, which is a useful label when trying to plan a bike-friendly route through an unfamiliar area.

## GTFS

GTFS stands for [General Transit Feed Specification](https://en.wikipedia.org/wiki/General_Transit_Feed_Specification).  It is a file format for sharing public transit schedules and route data. It is crucial in the creation of any trip planning software.  Most public transit organizations make their feeds publicably available.  The GTFS feed for Delaware can be foudn at the [DelDOT website] (https://www.dartfirststate.com/information/routes/).

## R5

R5 stands for "[Rapid Realistic Routing on Real-world and Reimagined](https://github.com/conveyal/r5)" networks. It was built as an efficient multi-modal route planning package written in java that can serve as the backend to routing applications.  The softare is well-maintained by developers of a company called [Conveyal](https://www.conveyal.com) who specializes in providing transit routing consulting services and products desgined for large-scale implementation by [cities and metro areas all over the world](https://www.conveyal.com/clients/).  Despite being a for-profit company, Conveyal maintains a very extensive [github with numerous open source repositories](https://github.com/conveyal) that are licensed for free use.  Their business model is to provide consulting services for organizations who are looking to increase the efficiency of their existing infrastructure or their expand infrastructure in ways that will best meet the needs of their local communities.  This business model allows them to provide and maintain many of these open source repositories.

## Modeify

[Modeify](https://github.com/conveyal/modeify) is an open source multi-modal trip planning platform that is also maintained by Conveyal.  It's a frontend app built using [Node.js](https://nodejs.org/en/) and connects to R5 on the backend.  It features a similar map interface as the likes of google maps, but it provides the signficant new capability of combining biking with public transport as a multi-modal transport option in addition to providing single-modal trip planning.

## ModeifyReactNative
[ModeifyReactNative](https://github.com/conveyal/ModeifyReactNative) is a mobile version of Modeify.  It is built using [React Native](https://facebook.github.io/react-native/), which lets developers effectively code once and deploy simultaneously to both Android and iOS.  This makes apps coded in React Native accessible to most mobile phone owners.  ModeifyReactNative has already been used to create the mobiel app for CarFreeAtoZ, an implementation of these 3 Conveyal Repos for the Washington DC - Arlington Metro Area.  Our goal is demonstrate that we can bring the same full-stack multi-modal trip planning solution to the State of Delaware.

# Setup

## Building R5 for DE

First we need to pull the DelDOT GTFS data and get an OpenStreetMap .pbf file for the state of delaware.  These are the data files that R5 will use to find routes.

```shell
$ mkdir ~/Downloads/delaware_r5
$ cd ~/Downloads/delaware_r5
$ curl http://download.geofabrik.de/north-america/us/delaware-latest.osm.pbf -o delaware-latest.osm.pbf
$ curl -LO http://dartfirststate.com/information/routes/gtfs_data/dartfirststate_de_us.zip -o dartfirststate_de_us.zip
```

R5 runs as a standalone server that can be queried using an API endpoint or through the Modeify frontend.  To compile R5 from the repo requires first ensuring that [Apache Maven](https://maven.apache.org/install.html) and [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) are installed properly.  Maven also requires that the JAVA_HOME enviroment variable points to the JDK and not the JRE installation.

After installing, check the Maven and Java installations.
```sh
$ mvn -v
$ java -version
```

To build the r5 jar using maven
```
git clone  https://github.com/conveyal/r5.git r5-3.2.0
cd r5-3.2.0
git checkout tags/v3.4.1
mvn package -DskipTests
```

The following will build R5 for DE usign the OSM .pbf and DelDOT GTFS files.  Make sure to change the path to where you downloaded those files

```
java -Xmx1G -cp target/v3.4.1.jar com.conveyal.r5.R5Main point --build /Users/name/Downloads/delaware_r5
java -Xmx1G -cp target/v3.4.1.jar com.conveyal.r5.R5Main point --graphs /Users/name/Downloads/delaware_r5
```

Once it is finished, you should see some text in the console to indicate that R5 is running on port 8080.  You can query R5 to check that it is functioning properly:

```
curl 'http://localhost:8080/otp/routers/default/index/graphql' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/json; charset=UTF-8' --data-binary '{"query":"query requestPlan($fromLat:Float!, $fromLon:Float!,\n $toLat:Float!, $toLon:Float!, $wheelchair:Boolean\n $fromTime: ZonedDateTime!, $toTime:ZonedDateTime!,\n $bikeTrafficStress:Int!, $minBikeTime:Int!\n ) {\nplan(fromLat:$fromLat, fromLon:$fromLon, toLat:$toLat,toLon:$toLon, wheelchair:$wheelchair, fromTime:$fromTime, toTime:$toTime, \n \n directModes:[WALK,BICYCLE,CAR],\n accessModes:[WALK,BICYCLE,CAR_PARK], egressModes:[WALK],\ntransitModes:[BUS],\nbikeTrafficStress: $bikeTrafficStress, minBikeTime: $minBikeTime) {\n patterns {\n tripPatternIdx\n trips {\n tripId\n serviceId\n wheelchairAccessible\n bikesAllowed\n }\n }\n options {\n summary,\n fares {\n type,low,peak,senior, transferReduction, currency\n }\n itinerary {\n waitingTime\n walkTime\n distance\n transfers\n duration\n transitTime\n startTime\n endTime,\n connection {\n access\n egress,\n transit {\n pattern\n time\n }\n }\n \n }\n \n \n transit {\n from {\n name,\n stopId,\n lon,\n lat,\n wheelchairBoarding\n },\n to {\n name,\n stopId,\n lon,\n lat,\n wheelchairBoarding\n },\n mode,\n routes {id, routeIdx shortName, mode, agencyName},\n segmentPatterns {\n patternId, patternIdx,\n\trouteIdx\n fromIndex\n toIndex,\n fromDepartureTime\n toArrivalTime,\n tripId\n \n \n },\n middle {\n mode,\n duration,\n distance,\n geometryGeoJSON,\n }\n },\n \n access {\n mode,\n duration,\n distance,\n #geometryGeoJSON,\n \n #elevation {\n #distance\n #elevation\n #},\n #alerts {\n #alertHeaderText\n #alertDescriptionText\n #alertUrl\n #effectiveStartDate\n #effectiveEndDate\n #},\n streetEdges {\n #edgeId\n distance\n geometryGeoJSON\n mode\n streetName\n relativeDirection\n absoluteDirection\n #stayOn\n #area\n #exit\n #bogusName,\n bikeRentalOnStation {\n id,\n name,\n lat,lon\n }\n bikeRentalOffStation {\n id,\n name,\n lat,lon\n }\n parkRide {\n id,\n name,\n lat,lon\n }\n }\n },\n egress {\n mode,\n duration,\n distance,\n geometryGeoJSON,\n \n #elevation {\n #distance\n #elevation\n #},\n #alerts {\n #alertHeaderText\n #alertDescriptionText\n #alertUrl\n #effectiveStartDate\n #effectiveEndDate\n #},\n streetEdges {\n #edgeId\n #distance\n #geometryPolyline\n mode\n geometryGeoJSON,\n streetName\n relativeDirection\n absoluteDirection\n #stayOn\n #area\n #exit\n #bogusName,\n #bikeRentalOnStation {\n #id,\n #name,\n #lon,lat\n #}\n }\n }\n } \n }\n \n #patterns{patternId, startTime, endTime, realTime, arrivalDelay, departureDelay}\n} \n","variables":"{\"fromLat\":\"39.828905\",\"fromLon\":\"-75.538053\",\"toLat\":\"39.806424\",\"toLon\":\"-75.552387\",\"wheelchair\":false,\"fromTime\":\"2018-04-23T07:00-04:00\",\"toTime\":\"2018-04-23T09:00-04:00\",\"minBikeTime\":5,\"bikeTrafficStress\":4}"}' --compressed
```

## Modeify setup

Modeify runs on [Node.js v6.x](https://nodejs.org/en/download/releases/).  Instead of using the default npm package to install, [yarn](https://yarnpkg.com/lang/en/docs/install/) must be used instead. Once those are installed, you are good to go!

Clone the modeify git repo

```shell
git clone https://github.com/conveyal/modeify.git modeify-v1.3.0
cd modeify-v1.3.0
git checkout tags/v1.3.0
```

There are several configuration files that need to be edited to replace the various access tokens including tokens/keys for MongoDB, Mapbox, Mapzen Search, and Auth0.  To protect our own accounts with these services, we have placed dummy values in these fields in the files in the modeify-files folder.  For these services, we were able use the free tier or asked the modeify developers for access, and they were happy to oblige.  More details for what lines in these files need to be edited can be found at the [Modeify repo readme] (https://github.com/conveyal/modeify)

```shell
git clone https://github.com/opendatachallenge2018/2018-odc-final/modeify-files.git modeify-files
mv /modeify-files/settings.yml /home/ec2-user/modeify-v1.3.0/configurations/default/settings.yml
mv /modeify-files/env.yml /home/ec2-user/modeify-v1.3.0/configurations/default/env.yml
```

Here is where you must install using yarn then startup using npm which is installed with node.
```shell
yarn install
npm start
```

Modeify can then be accessed in your brower on port 5000

``` $ open http://localhost:5000/planner```

## ModeifyReactNative

Similary to Modeify, the two json configuration files need to have proper credentials written in for teh dummy placeholders.  Other than that, the ModeifyReactNative is started just as easily with yarn and npm

```
git clone https://github.com/conveyal/ModeifyReactNative.git modeify-react-native-v2
sudo mv /modeify-files/config.json /modeify-react-native-v2/config.json
sudo mv /modeify-files/package.json /modeify-react-native-v2/package.json
cd modeify-react-native-v2
yarn install
npm start
```

## Hosting on AWS

All three team members were running different setups on their local machines which made it hard for everyone to install the right dependencies properly.  What we have done is figure out how to configure Linux EC2's on Amazon Web Services using a pair of bash scripts in the EC2-Scripts folder.  We have verified that this will install the right dependencies on an AWS EC2 t2.  Again, they still require that the dummy placeholders in the configuration files are changed.  Unfortunately, the program runs extremely slow on a AWS free tier t2.micro, which only had 1GB of RAM and 1 CPU.  However, it is only 5 cents per hour to run a t2.medium and we currently have Modeify and R5 both running on one of these at the moment. We already had a run in with AWS keys getting out and used to create a few hundred large instances, casuing one of our accounts to be locked so we are hesitant to share this right now.  Message me if you want to test out our working instance.





